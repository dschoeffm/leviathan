#include "ruleset.hpp"
#include <iostream>

#include <pcap.h>

#include "llvmStructs.hpp"
#include "ruleset.hpp"

#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/ExecutionEngine/GenericValue.h"
#include "llvm/ExecutionEngine/MCJIT.h"
#include "llvm/IR/Argument.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/Casting.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/raw_ostream.h"
#include <exception>
#include <functional>

using namespace std;
using namespace llvm;

void print_usage(char *prog_name) {
  cout << prog_name << " <filename>" << endl;
};

std::function<int(void *)> createContext(Ruleset &rules) {
  InitializeNativeTarget();
  InitializeNativeTargetAsmPrinter();
  LLVMContext Context;

  // Create some module to put our function into it.
  std::unique_ptr<Module> Owner(new Module("FirewallFunction", Context));
  Module *M = Owner.get();

  // We are about to create the firewall function:
  Function *FirewallFunc = rules.get_eval_func(M, Context);

  // Now we going to create JIT
  std::string errStr;
  ExecutionEngine *EE = EngineBuilder(std::move(Owner))
                            .setEngineKind(EngineKind::JIT)
                            .setErrorStr(&errStr)
                            .create();

  if (!EE) {
    throw new std::runtime_error(
        string("Failed to construct ExecutionEngine: ") + errStr);
  }

  errs() << "OK\n";
  errs() << "We just constructed this LLVM module:\n\n---------\n" << *M;

  errs() << "verifying... ";
  if (verifyModule(*M)) {
    throw new std::runtime_error("Error constructing function");
  }
  errs() << "verification okay :)\n";

  std::function<int(void *)> fun =
      (int (*)(void *))EE->getFunctionAddress(FirewallFunc->getName());

  return fun;
}

int main(int argc, char **argv) {
  pcap_t *descr;
  char errbuf[PCAP_ERRBUF_SIZE];

  if (argc != 2) {
    print_usage(argv[0]);
    return 1;
  }

  // open capture file for offline processing
  descr = pcap_open_offline(argv[1], errbuf);
  if (descr == NULL) {
    cout << "pcap_open_offline() failed: " << errbuf << endl;
    return 1;
  }

  return 0;
}
