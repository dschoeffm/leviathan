#include "ruleset.hpp"

using namespace llvm;

#define PREFIX_MASK(len) ((uint32_t)(~((((uint64_t)1) << (32 - len)) - 1)))

Value *Subnet::build_match(IRBuilder<> &Builder, LLVMContext &ctx,
                           Value *ip_addr) {
  uint32_t mask = PREFIX_MASK(this->len);
  Value *mask_llvm = ConstantInt::get(Type::getInt32Ty(ctx), mask);
  Value *and_res = Builder.CreateAnd(ip_addr, mask_llvm, "mask_ip_addr");
  Value *addr_llvm = ConstantInt::get(Type::getInt32Ty(ctx), this->addr);
  Value *xor_res = Builder.CreateXor(and_res, addr_llvm, "xor_addresses");
  return xor_res;
};

Value *SrcIPMatch::build_match(IPv4Interface &header) {
  Value *srcIP = header.getSrcIP();
  return this->addr.build_match(this->Builder, this->ctx, srcIP);
};

Value *DstIPMatch::build_match(IPv4Interface &header) {
  Value *srcIP = header.getDstIP();
  return this->addr.build_match(this->Builder, this->ctx, srcIP);
};

Value *IPVersionMatch::build_match(IPv4Interface &header) {
  Value *header_version = header.getVersion();
  Value *xor_res =
      Builder.CreateXor(header_version, this->version, "check_IP_version");
  return xor_res;
};

Value *Rule::build_rule(llvm::LLVMContext &Context,
                        llvm::IRBuilder<> &Builder) {
  for (auto match : this->matches) {
  }
};

void Ruleset::add_rule(std::shared_ptr<Rule>){

};

Function *Ruleset::get_eval_func(llvm::Module *M, llvm::LLVMContext &Context){

};
