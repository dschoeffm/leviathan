#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Type.h>

#include "llvmStructs.hpp"

#include <vector>

using namespace llvm;

/*
 * IPv4
 */

llvm::StructType *getIPv4Header(llvm::LLVMContext &ctx) {
  llvm::StructType *v4Header = llvm::StructType::create(ctx, "IPv4Header");
  std::vector<llvm::Type *> elements;
  elements.push_back(llvm::Type::getIntNTy(ctx, 4));  // Version
  elements.push_back(llvm::Type::getIntNTy(ctx, 4));  // IHL
  elements.push_back(llvm::Type::getIntNTy(ctx, 6));  // DSCP
  elements.push_back(llvm::Type::getIntNTy(ctx, 2));  // ECN
  elements.push_back(llvm::Type::getInt16Ty(ctx));    // Total Length
  elements.push_back(llvm::Type::getInt16Ty(ctx));    // Identification
  elements.push_back(llvm::Type::getIntNTy(ctx, 3));  // Flags
  elements.push_back(llvm::Type::getIntNTy(ctx, 13)); // Fragment Offset
  elements.push_back(llvm::Type::getInt8Ty(ctx));     // TTL
  elements.push_back(llvm::Type::getInt8Ty(ctx));     // Protocol
  elements.push_back(llvm::Type::getInt16Ty(ctx));    // Checksum
  elements.push_back(llvm::Type::getInt32Ty(ctx));    // Src IP
  elements.push_back(llvm::Type::getInt32Ty(ctx));    // Dst IP
  v4Header->setBody(elements, true);

  // This is just debugging
  // v4Header->dump();

  return v4Header;
};

Value *IPv4Interface::getVersion() {
  Value *ptr = this->Builder.CreateStructGEP(this->type, this->header, 0,
                                             "IP_Version_ptr");
  return this->Builder.CreateLoad(ptr, "IP_Version");
};

Value *IPv4Interface::getIHL() {
  Value *ptr =
      this->Builder.CreateStructGEP(this->type, this->header, 1, "IP_IHL_ptr");
  return this->Builder.CreateLoad(ptr, "IP_IHL");
};

Value *IPv4Interface::getDSCP() {
  Value *ptr =
      this->Builder.CreateStructGEP(this->type, this->header, 2, "IP_DSCP_ptr");
  return this->Builder.CreateLoad(ptr, "IP_DSCP");
};

Value *IPv4Interface::getECN() {
  Value *ptr =
      this->Builder.CreateStructGEP(this->type, this->header, 3, "IP_ECN_ptr");
  return this->Builder.CreateLoad(ptr, "IP_ECN");
};

Value *IPv4Interface::getTotalLen() {
  Value *ptr = this->Builder.CreateStructGEP(this->type, this->header, 4,
                                             "IP_TotalLen_ptr");
  return this->Builder.CreateLoad(ptr, "IP_TotalLen");
};

Value *IPv4Interface::getIdent() {
  Value *ptr = this->Builder.CreateStructGEP(this->type, this->header, 5,
                                             "IP_Ident_ptr");
  return this->Builder.CreateLoad(ptr, "IP_Ident");
};

Value *IPv4Interface::getFlags() {
  Value *ptr = this->Builder.CreateStructGEP(this->type, this->header, 6,
                                             "IP_Flags_ptr");
  return this->Builder.CreateLoad(ptr, "IP_Flags");
};

Value *IPv4Interface::getFragmentOffset() {
  Value *ptr = this->Builder.CreateStructGEP(this->type, this->header, 7,
                                             "IP_FragmentOffset_ptr");
  return this->Builder.CreateLoad(ptr, "IP_FragmentOffset");
};

Value *IPv4Interface::getTTL() {
  Value *ptr =
      this->Builder.CreateStructGEP(this->type, this->header, 8, "IP_TTL_ptr");
  return this->Builder.CreateLoad(ptr, "IP_TTL");
};

Value *IPv4Interface::getProtocol() {
  Value *ptr = this->Builder.CreateStructGEP(this->type, this->header, 8,
                                             "IP_Proto_ptr");
  return this->Builder.CreateLoad(ptr, "IP_Proto");
};

Value *IPv4Interface::getChecksum() {
  Value *ptr = this->Builder.CreateStructGEP(this->type, this->header, 9,
                                             "IP_Checksum_ptr");
  return this->Builder.CreateLoad(ptr, "IP_Checksum");
};

Value *IPv4Interface::getSrcIP() {
  Value *ptr = this->Builder.CreateStructGEP(this->type, this->header, 10,
                                             "IP_SrcIP_ptr");
  return this->Builder.CreateLoad(ptr, "IP_SrcIP");
};

Value *IPv4Interface::getDstIP() {
  Value *ptr = this->Builder.CreateStructGEP(this->type, this->header, 11,
                                             "IP_DstIP_ptr");
  return this->Builder.CreateLoad(ptr, "IP_DstIP");
};

/*
 * Ethernet
 */

llvm::StructType *getEtherHeader(llvm::LLVMContext &ctx) {
  llvm::StructType *etherHeader = llvm::StructType::create(ctx, "EtherHeader");
  std::vector<llvm::Type *> elements;
  elements.push_back(
      llvm::ArrayType::get(llvm::Type::getInt8Ty(ctx), 6)); // Destination
  elements.push_back(
      llvm::ArrayType::get(llvm::Type::getInt8Ty(ctx), 6)); // Source
  elements.push_back(llvm::Type::getInt16Ty(ctx));          // Ethertype

  etherHeader->setBody(elements, true);

  // This is just debugging
  // etherHeader->dump();

  return etherHeader;
};

Value *EtherInterface::getDst() {
  Value *ptr = this->Builder.CreateStructGEP(this->type, this->header, 0,
                                             "Ether_Dst_ptr");
  return this->Builder.CreateLoad(ptr, "Ether_Dst");
};

Value *EtherInterface::getSrc() {
  Value *ptr = this->Builder.CreateStructGEP(this->type, this->header, 1,
                                             "Ether_Src_ptr");
  return this->Builder.CreateLoad(ptr, "Ether_Src");
};

Value *EtherInterface::getEthertype() {
  Value *ptr = this->Builder.CreateStructGEP(this->type, this->header, 2,
                                             "Ether_Type_ptr");
  return this->Builder.CreateLoad(ptr, "Ether_Type");
};
