#ifndef PACKET_HPP
#define PACKET_HPP

#include <cstdint>

struct Packet {
  uint8_t *packet_data;
  uint16_t packet_length;
};

#endif /* PACKET_HPP */
