#ifndef LLVMSTRUCTS_HPP
#define LLVMSTRUCTS_HPP

#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Type.h>

#include <vector>

/*
 * IPv4
 */

llvm::StructType *getIPv4Header(llvm::LLVMContext &ctx);

class IPv4Interface {
private:
  llvm::IRBuilder<> &Builder;
  llvm::Value *header;
  llvm::LLVMContext &ctx;
  llvm::StructType *type;

public:
  IPv4Interface(llvm::IRBuilder<> &Builder, llvm::Value *header,
                llvm::LLVMContext &ctx)
      : Builder(Builder), header(header), ctx(ctx), type(getIPv4Header(ctx)){};

  llvm::Value *getVersion();
  llvm::Value *getIHL();
  llvm::Value *getDSCP();
  llvm::Value *getECN();
  llvm::Value *getTotalLen();
  llvm::Value *getIdent();
  llvm::Value *getFlags();
  llvm::Value *getFragmentOffset();
  llvm::Value *getTTL();
  llvm::Value *getProtocol();
  llvm::Value *getChecksum();
  llvm::Value *getSrcIP();
  llvm::Value *getDstIP();
};

/*
 * Ethernet
 */

llvm::StructType *getEtherHeader(llvm::LLVMContext &ctx);

class EtherInterface {
private:
  llvm::IRBuilder<> &Builder;
  llvm::Value *header;
  llvm::LLVMContext &ctx;
  llvm::StructType *type;

public:
  EtherInterface(llvm::IRBuilder<> &Builder, llvm::Value *header,
                 llvm::LLVMContext &ctx)
      : Builder(Builder), header(header), ctx(ctx), type(getEtherHeader(ctx)){};

  llvm::Value *getDst();
  llvm::Value *getSrc();
  llvm::Value *getEthertype();
};

#endif /* LLVMSTRUCTS_HPP */
