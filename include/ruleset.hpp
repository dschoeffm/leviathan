#ifndef RULESET_HPP
#define RULESET_HPP

#include "llvmStructs.hpp"
#include "packet.hpp"

#include <llvm/ExecutionEngine/GenericValue.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>

#include <memory>
#include <vector>

enum Action { ACCEPT, DROP, REJECT };

class Subnet {
private:
  uint32_t addr;
  uint8_t len;

public:
  Subnet(uint32_t addr, uint8_t len) : addr(addr), len(len){};
  uint32_t get_addr() { return addr; }
  uint32_t get_len() { return len; }
  llvm::Value *build_match(llvm::IRBuilder<> &Builder, llvm::LLVMContext &ctx,
                           llvm::Value *ip_addr);
};

class Match {
protected:
  llvm::IRBuilder<> &Builder;
  llvm::LLVMContext &ctx;

public:
  Match(llvm::IRBuilder<> &Builder, llvm::LLVMContext &ctx)
      : Builder(Builder), ctx(ctx){};
};

class SrcIPMatch : public Match {
private:
  Subnet addr;

public:
  SrcIPMatch(llvm::IRBuilder<> &Builder, llvm::LLVMContext &ctx, Subnet addr)
      : Match(Builder, ctx), addr(addr){};
  llvm::Value *build_match(IPv4Interface &header);
};

class DstIPMatch : public Match {
private:
  Subnet addr;

public:
  DstIPMatch(llvm::IRBuilder<> &Builder, llvm::LLVMContext &ctx, Subnet addr)
      : Match(Builder, ctx), addr(addr){};
  llvm::Value *build_match(IPv4Interface &header);
};

/*
class SrcPortMatch : public Match {
private:
  uint16_t port;

public:
  SrcPortMatch(llvm::IRBuilder<> &Builder, llvm::LLVMContext &ctx,
               uint16_t port)
      : Match(Builder, ctx), port(port){};
  llvm::Value *build_match(IPv4Interface &header);
};

class DstPortMatch : public Match {
private:
  uint16_t port;

public:
  DstPortMatch(llvm::IRBuilder<> &Builder, llvm::LLVMContext &ctx,
               uint16_t port)
      : Match(Builder, ctx), port(port){};
  llvm::Value *build_match(IPv4Interface &header);
};
*/

class IPVersionMatch : public Match {
private:
  uint16_t version;

public:
  IPVersionMatch(llvm::IRBuilder<> &Builder, llvm::LLVMContext &ctx,
                 uint8_t version)
      : Match(Builder, ctx), version(version){};
  llvm::Value *build_match(IPv4Interface &header);
};

class Rule {
private:
  std::vector<std::shared_ptr<Match>> matches;
  bool quick;
  Action action;

public:
  Rule(){};
  void add_match(std::shared_ptr<Match>);
  llvm::Value *build_rule(llvm::LLVMContext &Context,
                          llvm::IRBuilder<> &Builder);
};

class Ruleset {
private:
  std::vector<std::shared_ptr<Rule>> rules;

public:
  Ruleset(){};

  void add_rule(std::shared_ptr<Rule>);

  llvm::Function *get_eval_func(llvm::Module *M, llvm::LLVMContext &Context);
};

#endif /* RULESET_HPP */
